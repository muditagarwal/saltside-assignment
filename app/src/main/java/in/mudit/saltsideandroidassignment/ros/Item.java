package in.mudit.saltsideandroidassignment.ros;

import io.realm.RealmObject;

/**
 * Created by muditagarwal on 03/10/16.
 */
public class Item extends RealmObject {

    private String title;
    private String description;
    private String image;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}
