/*
 * Copyright 2016 Realm Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.mudit.saltsideandroidassignment.ui.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.mudit.saltsideandroidassignment.R;
import in.mudit.saltsideandroidassignment.ros.Item;
import in.mudit.saltsideandroidassignment.ui.activities.DetailsActivity;
import in.mudit.saltsideandroidassignment.ui.activities.MainActivity;
import in.mudit.saltsideandroidassignment.utils.AppConstants;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class MyRecyclerViewAdapter extends RealmRecyclerViewAdapter<Item, MyRecyclerViewAdapter.MyViewHolder> {

    private final MainActivity activity;

    public MyRecyclerViewAdapter(MainActivity activity, OrderedRealmCollection<Item> data) {
        super(activity, data, true);
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item obj = getData().get(position);
        holder.data = obj;
        holder.title.setText(obj.getTitle());
        holder.description.setText(obj.getDescription());
        holder.itemView.setTag(obj);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, description;
        public Item data;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.description);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            Item item = (Item) v.getTag();
            Intent i = new Intent(activity, DetailsActivity.class);
            i.putExtra(AppConstants.EXTRA_TITLE, item.getTitle());
            i.putExtra(AppConstants.EXTRA_DESC, item.getDescription());
            i.putExtra(AppConstants.EXTRA_IMAGE, item.getImage());
            activity.startActivity(i);
        }
    }


}