package in.mudit.saltsideandroidassignment.ui.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import in.mudit.saltsideandroidassignment.R;
import in.mudit.saltsideandroidassignment.ros.Item;
import in.mudit.saltsideandroidassignment.services.DownloadItemsService;
import in.mudit.saltsideandroidassignment.ui.adapters.MyRecyclerViewAdapter;
import in.mudit.saltsideandroidassignment.utils.AppConstants;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Realm realm;
    private ProgressDialog progressDialog;

    private ResponseReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Getting items");

        realm = Realm.getDefaultInstance();
        setUpRecyclerView();

        loadItems();
    }

    private void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new MyRecyclerViewAdapter(this, realm.where(Item.class).findAllAsync()));
        recyclerView.setHasFixedSize(true);
    }

    private void loadItems() {
        DownloadItemsService.startDownloadOfItems(this);
        if (realm.where(Item.class).count() <= 0) {
            showProgress();

            IntentFilter filter = new IntentFilter(DownloadItemsService.ACTION_DOWNLOAD_ITEMS);
            filter.addCategory(Intent.CATEGORY_DEFAULT);
            receiver = new ResponseReceiver();
            registerReceiver(receiver, filter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
    }

    public void showProgress() {
        progressDialog.show();
    }

    public void hideProgress() {
        progressDialog.hide();
    }

    public class ResponseReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            hideProgress();
            if (!intent.getBooleanExtra(AppConstants.EXTRA_STATUS, false)) {
                Snackbar.make(recyclerView, "No Items found!", Snackbar.LENGTH_INDEFINITE).setAction("Reload", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadItems();
                    }
                }).show();
            }
        }
    }

}