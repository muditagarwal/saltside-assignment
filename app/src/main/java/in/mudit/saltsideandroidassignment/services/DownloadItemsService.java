package in.mudit.saltsideandroidassignment.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import in.mudit.saltsideandroidassignment.network.APIManager;
import in.mudit.saltsideandroidassignment.ros.Item;
import in.mudit.saltsideandroidassignment.utils.AppConstants;
import io.realm.Realm;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class DownloadItemsService extends IntentService {

    public static final String ACTION_DOWNLOAD_ITEMS = "in.mudit.saltsideandroidassignment.ros.action.DOWNLOAD_ITEMS";

    public DownloadItemsService() {
        super("DownloadItemsService");
    }

    /**
     * Starts this service to download items. If the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startDownloadOfItems(Context context) {
        Intent intent = new Intent(context, DownloadItemsService.class);
        intent.setAction(ACTION_DOWNLOAD_ITEMS);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            boolean status = false;
            try {
                List<Item> items = APIManager.getApiManager().getItems();

                // Open the Realm
                Realm realm = Realm.getDefaultInstance();
                // Work with Realm
                realm.beginTransaction();
                realm.delete(Item.class);
                realm.copyToRealm(items);
                realm.commitTransaction();

                //close realm
                realm.close();
                status = true;
            } catch (IOException e) {
                Log.e("DownloadService", e.getMessage(), e);
            } finally {
                // send a broadcast to hide progress
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction(ACTION_DOWNLOAD_ITEMS);
                broadcastIntent.putExtra(AppConstants.EXTRA_STATUS, status);
                broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
                sendBroadcast(broadcastIntent);
            }
        }
    }

}
