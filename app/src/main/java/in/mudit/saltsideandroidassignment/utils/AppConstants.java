package in.mudit.saltsideandroidassignment.utils;

/**
 * Created by muditagarwal on 03/10/16.
 */
public class AppConstants {

    public static final String EXTRA_IMAGE = "IMAGE";
    public static final String EXTRA_TITLE = "TITLE";
    public static final String EXTRA_DESC = "DESC";
    public static final String EXTRA_STATUS = "STATUS";

}
