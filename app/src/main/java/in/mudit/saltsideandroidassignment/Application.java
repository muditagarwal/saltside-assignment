package in.mudit.saltsideandroidassignment;

import io.realm.Realm;

/**
 * Created by muditagarwal on 03/10/16.
 */
public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
