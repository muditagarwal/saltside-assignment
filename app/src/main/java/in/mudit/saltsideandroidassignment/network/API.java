package in.mudit.saltsideandroidassignment.network;

import java.util.List;

import in.mudit.saltsideandroidassignment.ros.Item;
import retrofit2.Call;
import retrofit2.http.GET;

public interface API {

    @GET("items.json")
    Call<List<Item>> getItems();

}