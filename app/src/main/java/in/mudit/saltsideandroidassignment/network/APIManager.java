package in.mudit.saltsideandroidassignment.network;

import java.io.IOException;
import java.util.List;

import in.mudit.saltsideandroidassignment.ros.Item;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by muditagarwal on 03/10/16.
 */
public class APIManager {

    private API service;
    private static APIManager apiManager;

    private APIManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://gist.githubusercontent.com/maclir/f715d78b49c3b4b3b77f/raw/8854ab2fe4cbe2a5919cea97d71b714ae5a4838d/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(API.class);
    }

    public static APIManager getApiManager() {
        if (apiManager == null) {
            apiManager = new APIManager();
        }
        return apiManager;
    }

    public List<Item> getItems() throws IOException {
        Call<List<Item>> callGetItems = service.getItems();
        Response<List<Item>> getItemsResponse = callGetItems.execute();
        return getItemsResponse.body();
    }

}